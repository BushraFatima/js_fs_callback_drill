/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');

function fsProblem2(file) {    

    const fileNames = '/home/bf/Desktop/Callbacks-Drill-1/namesFile.txt';

    fs.readFile(file, 'utf8', (err, data) => {

        if (err) {
            console.log(err);
        }
        else {
            console.log('File read');

            let upperFile = '/home/bf/Desktop/Callbacks-Drill-1/upper.txt';

            fs.writeFile(upperFile, data.toUpperCase(), (err) => { 

                if (err) {
                    console.log(err);
                }
                else {
                    console.log('File created');

                    fs.appendFile(fileNames, 'upper.txt\n', (err) => {     

                        if (err) {
                            console.log(err);
                        }
                        else {
                            console.log("File name added.");

                            fs.readFile(upperFile, 'utf8', (err, data) => {    

                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    let lowerSplitFile = '/home/bf/Desktop/Callbacks-Drill-1/lower&split.txt';
                                    data = data.toLowerCase();
                                    data = data.split('. ');
                                    data = data.join('.\n')

                                    fs.writeFile(lowerSplitFile, data, (err) => {    

                                        if (err) {
                                            console.log(err);
                                        }
                                        else {
                                            console.log("File created.");

                                            fs.appendFile(fileNames, 'lower&split.txt\n', (err) => {  

                                                if (err) {
                                                    console.log(err);
                                                }
                                                else {
                                                    console.log("File name added.");

                                                    fs.readFile(lowerSplitFile, 'utf8', (err, data) => {   

                                                        if (err) {
                                                            console.log(err);
                                                        }
                                                        else {
                                                            data = data.split('\n');
                                                            data = (data.sort()).slice(3);
                                                            data = data.join('\n');

                                                            let sortFile = '/home/bf/Desktop/Callbacks-Drill-1/sort.txt';

                                                            fs.writeFile(sortFile, data, (err) => {   

                                                                if (err) {
                                                                    console.log(err);
                                                                }
                                                                else {
                                                                    console.log('File created');

                                                                    fs.appendFile(fileNames, 'sort.txt', (err) => {  

                                                                        if (err) {
                                                                            console.log(err);
                                                                        }
                                                                        else {
                                                                            console.log("File name added");
                                                                            
                                                                            fs.readFile(fileNames, 'utf8', (err, data) => {   

                                                                                if (err) {
                                                                                    console.log(err);
                                                                                }
                                                                                else {
                                                                                    console.log('All files created');

                                                                                    let names = data.split('\n');
                                                                                    let folder = '/home/bf/Desktop/Callbacks-Drill-1';

                                                                                    for(let index = 0; index < names.length; index++) {
        
                                                                                        fs.unlink(`${folder}/${names[index]}`, (err) => {
                                                                            
                                                                                            if(err) {
                                                                                                console.log(err);
                                                                                            } else {
                                                                                                console.log(`${names[index]} file deleted`);
                                                                                            } 
                                                                                        });
                                                                                    }
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

module.exports = fsProblem2;