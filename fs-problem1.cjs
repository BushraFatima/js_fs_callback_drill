/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 

    Ensure that the function is invoked as follows: 
        fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles)
*/

const fs = require('fs');

function create(folderPath, totalFiles, cb) {

    fs.mkdir(folderPath, (err) => {

        if (err) {
          cb(err);

        } else {
      
            console.log(`folder is created!`);

            for (let num = 1; num <= totalFiles; num++) {

                let filename = 'jsonFile' + num;

                fs.writeFile(`${folderPath}/${filename}.json`,'{ name: bushra, color: red}', (err) => {
                    
                    if(err) {
                        cb(err);
                    } else {
                        console.log('Updated file.');

                        if (num === totalFiles) {
                            console.log('All files created.');
                            cb(null);
                        }
                    }

                });
            
            }  
        }
    });   
}

function deleteFiles(folderPath, totalFiles, cb) {

    for (let num = 1; num <= totalFiles; num++) {
        let filename = 'jsonFile' + num;

        fs.unlink(`${folderPath}/${filename}.json`, (err) => {

            if(err) {
                cb(err);

            } else {
                console.log('deleted file.');

                if(num === totalFiles) {
                    console.log('All files deleted.');
                    cb(null);
                }
            }
        });
    }

}

function fsProblem1 (absolutePathOfRandomDirectory, randomNumberOfFiles) {
    
    let folderPath = absolutePathOfRandomDirectory;
    let totalFiles = randomNumberOfFiles;

    create(folderPath, totalFiles, (err) => {

        if (err) {
            console.log(err);
        } else {
            deleteFiles(folderPath, totalFiles, (err) => {

                if (err) {
                    console.log(err);
                } else {
                    console.log('Done')
                }
            });
        }
    });          
}


module.exports = fsProblem1;
